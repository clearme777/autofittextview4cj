<div align="center">
<h1>库名</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.4-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-0.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化/毕业-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-HOS/Cloud-brightgreen" style="display: inline-block;" />
</p>

## 介绍

简要的介绍库，包括主要完成什么功能，解决什么问题，符合什么标准，应用到哪些领域，有哪些主要的特点等。

### 项目特性

- 特性1

- 特性2

- 特性3

### 项目计划

介绍开发和维护等关键里程碑

## 项目架构

架构图文说明，包括模块说明、架构层次等详细说明。

### 源码目录

```shell
.
├── README.md             #整体介绍
├── doc                   #文档目录，包括设计文档，API接口文档等
│   ├── design.md         #设计文档
│   └── feature_api.md    #特性接口文档
├── src                   #源码目录
│   └── Template.cj       #描述关键代码文件的功能
└── test                  #测试代码目录
    ├── HLT
    └── LLT
```

### 接口说明

主要类和函数接口说明，详见 [API](./doc/feature_api.md)


## 使用说明
### 编译构建

描述具体的编译过程：

```shell
cpm update
cpm build
```

### 功能示例
#### xxx 功能示例

功能示例描述:

示例代码如下：

```cangjie
import xxx.*
main() {
 xxxx
}
```

执行结果如下：

```shell
xxx
```

#### xxxx 功能示例

功能示例描述:

示例代码如下：

```cangjie
import xxx.*
main() {
 xxxx
}
```

执行结果如下：

```shell
xxx
```

## 约束与限制
描述环境限制，版本限制，依赖版本等

## 开源协议
[xx License]()

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。